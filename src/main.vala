/* main.vala
 *
 * Copyright 2019 Adwait Rawat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main (string[] args)
{
	GnuTLS.X509.PrivateKey pk;
	pk = GnuTLS.X509.PrivateKey.create ();
	pk.generate (GnuTLS.PKAlgorithm.RSA, 0, 0);

	

	Ggit.Repository? repo = null;
/*
	var path = ".";
	if (args.length > 1) {
		path = args[1];
	}
	print ("Path: %s\n", path);

	var file = File.new_for_path (path);
	print ("File: %s\n", file.get_path());

	File? repo_dir = null;
	try {
		repo_dir = Ggit.Repository.discover (file);
		print ("1\n");
	} catch (Error e) {
		critical (e.message);
	}

	try {
		if (repo_dir != null) {
			print ("Repo_dir: %s\n", repo_dir.get_path ());

			repo = Ggit.Repository.open (repo_dir);
			print ("Is empty: %s\n", repo.is_empty ().to_string ());
			print ("2\n");

			var origin = repo.lookup_remote ("origin");
			print ("Remote name: %s\n", origin.get_name ());
		} else {
			print ("NULL\n");
		}
	} catch (Error e) {
		warning (e.message);
	}
*/
	return 0;
}
